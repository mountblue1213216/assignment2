let key = [];

function keys(obj) {
    for(let i in obj){
        key.push(i);
    }
    return key;
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
}

module.exports = keys;